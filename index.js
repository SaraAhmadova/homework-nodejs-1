let requestCount = 0;
const http = require('http');

const args = process.argv.splice(',');
const prefix = '--port=';
const portParam = args.filter((arg) => arg.startsWith(prefix));

const getPortNumber = () => {
  // Check for command-line argument for custom port
  if (portParam[0]) {
    return portParam[0].slice(prefix.length);
  }
  return 3000;
};

http
  .createServer((req, res) => {
    if (req.url === '/favicon.ico') {
      res.writeHead(204);
      res.end();
      return;
    }
    res.writeHead(200, { 'Content-type': 'application/json' });
    requestCount += 1;
    res.end(
      JSON.stringify({
        message: 'Request handled successfully',
        requestCount,
      }),
    );
  })
  .listen(getPortNumber());
